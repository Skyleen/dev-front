class GridGenerator {
  constructor(xAxis, yAxis) {
    this.xAxis = xAxis;
    this.yAxis = yAxis;
    this.el = document.querySelector('#app');

    this.run();
  }

  getRandomTimer() {
    return (Math.random() * (2 - 1) + 1) * 1000;
  }

  getRandomHexa() {
    return `#${Math.floor(Math.random()*16777215).toString(16)}`;
  }

  render() {
    const table = document.createElement('table');

    for (let i = 0; i < this.xAxis; i += 1) {
      const tr = document.createElement('tr');

      for (let j = 0; j < this.yAxis; j += 1) {
        const td = document.createElement('td');

        td.style.padding = "4px";
        td.style.border = "1px solid black";

        setInterval(() => {
          td.style.background = this.getRandomHexa();
        }, this.getRandomTimer());

        tr.appendChild(td);
      }

      table.appendChild(tr);
    }

    this.el.appendChild(table);
  }

  run() {
    this.render();
  }
}

const gridGenerator = new GridGenerator(50, 50);