class DrawBar {
  constructor(sum, nbr) {
    this.sum = sum;
    this.nbr = nbr;

    this.el = document.querySelector('#app');

    this.run();
  }

  render() {

    const pourcentage = (this.nbr * 100 / this.sum);

    app.innerHTML += `<progress style="width: 100%" id="file" max="100" value="${pourcentage}"></progress>`;
  }

  run() {
    this.render();
  }
}

const drawBar = new DrawBar(300, 180);