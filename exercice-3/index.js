class MyMorpionX0 {
  constructor(x, o) {
    this.el = document.querySelector('#app');
    this.currentPlayer = "X";
    this.x = 0;
    this.o = 0;
    this.tour = 0;

    this.run();
  }

  info() {
    return {
      x: this.x,
      o: this.o
    };
  }

  setValues(x, o) {
    this.x = x;
    this.o = o;
  }

  resetValues() {
    this.setValues(0, 0);
    this.restart();
  }

  restart() {
    this.el.innerHTML = "";
    this.tour = 0;
    this.render();
  }

  win(player) {
    setTimeout(() => {
      let leX = this.info().x;
      let leO = this.info().o;
      if(player == 'X'){
        leX++;
      } else if (player == 'O') {
        leO++;
      }
      this.setValues(leX, leO)
      if(leX == 3){
        window.alert(`${player} gagne la partie ${leX}-${leO}`);
        this.resetValues();
      } else if(leO == 3) {
        window.alert(`${player} gagne la partie ${leO}-${leX}`);
        this.resetValues();
      } else {
        window.alert(`${player} gagne le point`);
        this.restart();
      }
    }, 100);
  }

  matchNul() {
    setTimeout(() => {
      window.alert('Match nul');
      this.restart();
    }, 100);
  }

  searchWinner(player) {

    const searchId = (id) => {
      let doc = document.getElementById(`${id}`);
      if(doc !== null){
        return doc.innerHTML;
      }
    }

    const isValid = (id1, id2, id3) => {
      if(searchId(id1) === player &&
      searchId(id2) === player &&
      searchId(id3) === player) {
        document.getElementById(id1).style.background = "green";
        document.getElementById(id2).style.background = "green";
        document.getElementById(id3).style.background = "green";
        return true;
      }
    }

    // Lignes

    if(isValid("00", "01", "02") === true){ return true }
    if(isValid("10", "11", "12") === true){ return true }
    if(isValid("20", "21", "22") === true){ return true }

    // Colonnes

    if(isValid("00", "10", "20") === true){ return true }
    if(isValid("01", "11", "21") === true){ return true }
    if(isValid("02", "12", "22") === true){ return true }

    // Digonales

    if(isValid("00", "11", "22") === true){ return true }
    if(isValid("02", "11", "20") === true){ return true }
      
  }

  render() {
    app.innerHTML = `
      <div id='statutJeu'>
        <p><span>Player X : ${this.info().x}</span> Player O : ${this.info().o}</p>
      </div>
    `;

    const table = document.createElement('table');

    for (let i = 0; i < 3; i += 1) {
      const tr = document.createElement('tr');

      for (let j = 0; j < 3; j += 1) {
        const td = document.createElement('td');

        let endGame = false

        td.id = `${i}${j}`;

        td.style.padding = "30px";
        td.style.border = "1px solid black";

        const event = (e) => {
          e.currentTarget.textContent = this.currentPlayer;

          td.removeEventListener('click', event);
          
          endGame = this.searchWinner(this.currentPlayer);
          
          this.tour++;

          if(endGame === true){
            this.win(this.currentPlayer);
          } else if(this.tour == 9){
            this.matchNul();
          }
          
          if (this.currentPlayer === "X") {
            this.currentPlayer = "O";

            return;
          }

          this.currentPlayer = "X";
        }

        if(endGame == false){
          td.addEventListener('click', event);
        }

        tr.appendChild(td);
      }

      table.appendChild(tr);
    }

    this.el.appendChild(table);
    
  }


  run() {
    this.render();
  }
}

const myMorpionX0 = new MyMorpionX0();