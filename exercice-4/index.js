class WorldMap {
    constructor() {
      this.el = document.querySelector('#app');
  
      this.run();
    }

    render() {

        let paths = this.el.querySelectorAll('.map-svg path');
        let nomPays = this.el.querySelector('#liste-pays');

        paths.forEach((path) => {
            path.addEventListener('mouseenter', (e) => {
                if(e.target.style.fill != "red"){
                    e.target.style.fill = "blue";
                }
            });
            path.addEventListener('mouseleave', (e) => {
                if(e.target.style.fill != "red"){
                    e.target.style.fill = "grey";
                } 
            });
            path.addEventListener('click', (e) => {
                if(e.target.style.fill != "red"){
                    nomPays.innerHTML += `
                        <li id="pays-${e.currentTarget.id}">${e.currentTarget.id}</li>
                    `;
                    e.target.style.fill = "red";
                } else {
                    const id = document.querySelector(`#pays-${e.currentTarget.id}`);
                    id.remove();
                    e.target.style.fill = "blue";
                }
            });
        });

    }

    run() {
    this.render();
    }
}

const worldMap = new WorldMap();