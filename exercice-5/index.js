const form = document.querySelector('#form');

form.email.addEventListener('change', function() {
  validEmail(this);
});

form.password.addEventListener('change', function() {
  validPassword(this);
});

form.nom.addEventListener('change', function() {
  validNom(this, 'nom');
});

form.prenom.addEventListener('change', function() {
  validNom(this, 'prénom');
});

form.addEventListener('submit', function(e) {
  e.preventDefault();
  if(validEmail(form.email) && validPassword(form.password) && validNom(form.nom, 'nom') && validNom(form.prenom, 'prénom')) {
    form.submit();
  }
});

const validNom = (inputNom, type) => {
  let msg;
  let isValid = false;
  if(inputNom.value.length < 1) {
    msg = `Le ${type} doit contenir au moins 1 caractères`;
  } else if(/[0-9]/.test(inputNom.value)) {
    msg = `Le ${type} ne doit pas contenir de chiffre`;
  } else {
    msg = `Le ${type} est valide`;
    isValid = true;
  }

  let small = '';

  if(type == 'nom') {
    small = document.querySelector('#smallNom');
  } else if (type == 'prénom') {
    small = document.querySelector('#smallPrenom');
  }
    

  if(isValid == true) {
    small.innerHTML = msg;
    small.style.color = "green";
    return true;
  } else {
    small.innerHTML = msg;
    small.style.color = "red";
    return false;
  }
};

const validEmail = (inputEmail) => {
  const emailRegExp = new RegExp(
    '^[a-zA-Z0-9.-_]+[@]{1}[a-zA-Z0-9.-_]+[.]{1}[a-z]{2,10}$',
    'g'
  );

  const small = document.querySelector('#smallEmail');

  if(emailRegExp.test(inputEmail.value)) {
    small.innerHTML = "Adresse email valide";
    small.style.color = "green";
    return true;
  } else {
    small.innerHTML = "Adresse email non valide";
    small.style.color = "red";
    return false;
  }
};

const validPassword = (inputPassword) => {
  let msg;
  let isValid = false;

  if(inputPassword.value.length < 5) {
    msg = "Le mot de passe doit contenir au moins 5 caractères";
  } else if(!/[A-Z]/.test(inputPassword.value)) {
    msg = "Le mot de passe doit contenir au moins 1 majuscule";
  } else if (!/[a-z]/.test(inputPassword.value)) {
    msg = "Le mot de passe doit contenir au moins 1 minuscule";
  } else if (!/[0-9]/.test(inputPassword.value)) {
    msg = "Le mot de passe doit contenir au moins 1 chiffre";
  } else {
    msg = "Le mot de passe est valide";
    isValid = true;
  }

  const small = document.querySelector('#smallPassword');

  if(isValid == true) {
    small.innerHTML = msg;
    small.style.color = "green";
    return true;
  } else {
    small.innerHTML = msg;
    small.style.color = "red";
    return false;
  }
};