const checkPhoneNumber = () => {
  let format = new RegExp(
    '^(06|07|01)-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}$', 'g'
  );
  let phone = document.querySelector('#phone').value;
  let match = format.test(phone)
  console.log(match);
  document.querySelector('#valid').innerHTML = `${match}`;
};

console.log(checkPhoneNumber())