const computeNotes = (notes) => {

  const test = Array.isArray(notes);

  if(test == true) {
    let moyenne = 0;

    for(let i = 0; i < notes.length; i++) {
      moyenne = moyenne + notes[i];
    }

    moyenne = moyenne/notes.length;

    return moyenne;
  }

  return 0;

};

console.log(computeNotes([10, 13, 13, 12, 15, 12, 11, 16, 14]));