class Bateau {
  constructor(nom, cases, value){
    this.nom = nom;
    this.cases = cases;
    this.value = value;
    this.hit = 0;
  }

  getBateau() {
    return {
      nom: this.nom,
      cases: this.cases,
      value: this.value,
      hit: this.hit
    };
  }

  hitBateau() {
    this.hit++; 
  }

  resetBoat() {
    this.hit = 0;
  }
}

class BatailleNavale {
  constructor(matrice) {
    this.matrice = matrice;
    this.boats = [
      new Bateau("Torpilleur", 2, 1),
      new Bateau("Contre-tropilleur", 3, 2),
      new Bateau("Croiseur", 4, 3),
      new Bateau("Sous-marin", 5, 4),
      new Bateau("Porte-avion", 5, 5)
    ];
    this.el = document.querySelector('#app');

    this.run();
  }

  restart() {
    this.el.innerHTML = "";
    this.resetBoats();
    this.render();
  }

  resetBoats() {
    this.boats.forEach((boat) => {
      boat.resetBoat();
    });
  }

  formatMatrice() {    
    let alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    const len = this.matrice[0].length;
    const alphaIndex = [];
    let count = 0;
  

    for (let i = 0; i < len; i += 1) {
      if(i === 0){
        alphaIndex.push('');
      }

      alphaIndex.push(alpha[i]);

      if (alpha[i] === 'Z') {
        alpha.forEach((al, index) => alphaIndex.push(`${alpha[count]}${alpha[index]}`));

        count += 1;
      }
    }

    this.matrice.unshift(alphaIndex);

    this.matrice.forEach((ligne, index) => {
      if (index >= 1) {
        this.matrice[index].unshift(index);
      }
    });
  }

  render() {

    const infos = document.createElement('div');

    const info = document.createElement('p');
    infos.appendChild(info);
    info.textContent = 'Voici la liste des bateaux :';

    const listeBateau = document.createElement('ul')
    this.boats.forEach((boat) => {
      const unBateau = document.createElement('li')
      listeBateau.appendChild(unBateau);
      unBateau.setAttribute("id", `${boat.getBateau().value}`);
      unBateau.textContent = `${boat.getBateau().nom} de taille ${boat.getBateau().cases} touché ${boat.getBateau().hit} fois`
    });
    infos.appendChild(listeBateau);

    const alerte = document.createElement('p');
    infos.appendChild(alerte);
    alerte.setAttribute("id", "alerte");
    alerte.textContent = "Aucun bateau touché pour le moment..."

    this.el.appendChild(infos);

    const table = document.createElement('table');

    const boatsDown = [];

    for (let i = 0; i < this.matrice.length; i += 1) {
      const tr = document.createElement('tr');

      for (let j = 0; j < this.matrice[i].length; j += 1) {
        const td = document.createElement('td');

        td.style.padding = "4px";
        td.style.border = "1px solid black";

        if (i === 0 || j === 0) {
          td.textContent = this.matrice[i][j];
          tr.appendChild(td);

          continue;
        }

        td.style.background = "grey";

        td.setAttribute('data-value', this.matrice[i][j]);

        const event = (e) => {

          if (e.currentTarget.dataset.value != 0) {

            const value = e.currentTarget.dataset.value;
            let nomBateau = '';

            this.boats.forEach((boat) => {
              if(boat['value'] == value){
                nomBateau = boat.getBateau().nom;
                boat.hitBateau();

                if(boat.getBateau().hit === boat.getBateau().cases) {
                  document.getElementById(`${value}`).textContent = `${boat.getBateau().nom} coulé !`;
                  alerte.textContent = `Vous avez coulé le ${nomBateau} !`;
                  boatsDown.push(value);

                  if(boatsDown.length === 5) {
                    alerte.textContent = `Vous coulé tous les bateaux !`;
                    setTimeout(() => {
                      window.alert('Partie gagnée, trop fort !');
                      this.restart();
                    }, 100);
                  }

                  return;
                }

                document.getElementById(`${value}`).textContent = `${boat.getBateau().nom} de taille ${boat.getBateau().cases} touché ${boat.getBateau().hit} fois`;
                alerte.textContent = `Vous avez touché le ${nomBateau} !`;
              }
            });

            td.style.background = "red";
            
            td.removeEventListener('click', event);

            return;
          }

          td.style.background = "blue";

          alerte.textContent = `Plouf dans l'eau !`;

          td.removeEventListener('click', event);
        };

        td.addEventListener('click', event);

        tr.appendChild(td);
      }

      table.appendChild(tr);
    }

    this.el.appendChild(table);
  }

  run() {
    this.formatMatrice();
    this.render();
  }
}



/**
 * /!\ Mettre un bateau de chaque /!\
 * - Eau = 0
 * - Torpilleur = 1 -> 2 cases
 * - Contre-torpilleur = 2 -> 3 cases
 * - Croiseur = 3 -> 4 cases
 * - Sous-marin = 4 -> 5 cases
 * - Porte-Avion = 5 -> 5 cases
 * Placez vos bateaux
 *
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
 */

const batailleNavale = new BatailleNavale([
  [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
  [0, 3, 3, 3, 3, 0, 0, 0, 0, 0, 4, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]);