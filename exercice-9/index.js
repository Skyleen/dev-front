const checkPalindrome = (str) => {
  if(typeof(str) == 'string') {
    return str === str.split('').reverse().join('');
  }
};
console.log(checkPalindrome('sexes sexes'));